package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.gui.GUILocation;
import org.plugins.msquests.managers.GUIManager;

public class InventoryClickListener extends MSQuestListener {

    private GUIManager guiManager;

    public InventoryClickListener(MSQuests plugin, GUIManager guiManager) {
        super(plugin);
        this.guiManager = guiManager;

        register();
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (this.guiManager.inGUI(p)) {
            e.setCancelled(true);
            if (e.getClickedInventory() == p.getOpenInventory().getTopInventory()) {
                if (this.guiManager.getGUILocation(p) == GUILocation.MAIN) {
                    if (e.getSlot() == this.guiManager.getMainDailyQuestItemSlot()) {
                        this.guiManager.openGUI(p, GUILocation.DAILY);
                    } else if (e.getSlot() == this.guiManager.getMainWeeklyQuestItemSlot()) {
                        this.guiManager.openGUI(p, GUILocation.WEEKLY);
                    }
                } else if (e.getSlot() == this.guiManager.getBackSlot()) {
                    this.guiManager.openGUI(p, GUILocation.MAIN);
                }
            }
        }
    }
}
