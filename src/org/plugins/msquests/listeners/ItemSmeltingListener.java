package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.FurnaceExtractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.ItemsSmeltedQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class ItemSmeltingListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public ItemSmeltingListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onSmelt(FurnaceExtractEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null && dailyQuest.getQuestType() == QuestType.ITEMS_SMELTED) {
            Player p = e.getPlayer();
            if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                ItemsSmeltedQuest isQuest = dailyQuestManager.getItemsSmeltedQuest();
                if (isQuest.getSmeltItem().isSimilar(new ItemStack(e.getItemType()))) {
                    for (int i = 0; i < e.getItemAmount(); i++) {
                        if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                            dailyQuestManager.completeQuest(p);
                            break;
                        }
                    }
                }
            }
        }

        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null && weeklyQuest.getQuestType() == QuestType.ITEMS_SMELTED) {
            Player p = e.getPlayer();
            if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                ItemsSmeltedQuest isQuest = weeklyQuestManager.getItemsSmeltedQuest();
                if (isQuest.getSmeltItem().isSimilar(new ItemStack(e.getItemType()))) {
                    for (int i = 0; i < e.getItemAmount(); i++) {
                        if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                            weeklyQuestManager.completeQuest(p);
                            break;
                        }
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }


}
