package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.ItemsCraftedQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class ItemCraftListener extends MSQuestListener {

    private MSQuests plugin;
    private DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private PlayerManager playerManager;

    public ItemCraftListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onCraft(CraftItemEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null && dailyQuest.getQuestType() == QuestType.ITEMS_CRAFTED) {
            Player p = (Player) e.getWhoClicked();

            if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                ItemsCraftedQuest icQuest = dailyQuestManager.getItemsCraftedQuest();
                if (e.isShiftClick()) {
                    ItemStack item = e.getRecipe().getResult();

                    int realAmount = Integer.MAX_VALUE;
                    for (ItemStack matrixItem : e.getInventory().getMatrix()) {
                        if (matrixItem != null) {
                            if (matrixItem.getAmount() < realAmount) {
                                realAmount = matrixItem.getAmount();
                            }
                        }
                    }

                    if (item.isSimilar(icQuest.getCraftItem())) {
                        for (int i = 0; i < realAmount; i++) {
                            if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                                dailyQuestManager.completeQuest(p);
                                break;
                            }
                        }
                    }

                } else if (e.getCurrentItem().isSimilar(icQuest.getCraftItem())) {
                    if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                        dailyQuestManager.completeQuest(p);
                    }
                }
            }

        }

        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null && weeklyQuest.getQuestType() == QuestType.ITEMS_CRAFTED) {
            Player p = (Player) e.getWhoClicked();

            if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                ItemsCraftedQuest icQuest = weeklyQuestManager.getItemsCraftedQuest();
                if (e.isShiftClick()) {
                    ItemStack item = e.getRecipe().getResult();

                    int realAmount = Integer.MAX_VALUE;
                    for (ItemStack matrixItem : e.getInventory().getMatrix()) {
                        if (matrixItem != null) {
                            if (matrixItem.getAmount() < realAmount) {
                                realAmount = matrixItem.getAmount();
                            }
                        }
                    }

                    if (item.isSimilar(icQuest.getCraftItem())) {
                        for (int i = 0; i < realAmount; i++) {
                            if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                                weeklyQuestManager.completeQuest(p);
                                break;
                            }
                        }
                    }

                } else if (e.getCurrentItem().isSimilar(icQuest.getCraftItem())) {
                    if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                        weeklyQuestManager.completeQuest(p);
                    }
                }
            }

        }}
        }.runTaskAsynchronously(this.plugin);
    }

}
