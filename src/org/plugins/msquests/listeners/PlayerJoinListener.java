package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.GUIManager;
import org.plugins.msquests.managers.PlayerManager;

public class PlayerJoinListener extends MSQuestListener {

    private final MSQuests plugin;
    private final PlayerManager playerManager;
    private final GUIManager guiManager;

    public PlayerJoinListener(MSQuests plugin, PlayerManager playerManager, GUIManager guiManager) {
        super(plugin);
        this.plugin = plugin;
        this.playerManager = playerManager;
        this.guiManager = guiManager;

        register();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        this.playerManager.loadPlayer(p);
        this.guiManager.cloneMainGUI(p);
        this.guiManager.cloneDailyGUI(p);
        this.guiManager.cloneWeeklyGUI(p);
    }
}
