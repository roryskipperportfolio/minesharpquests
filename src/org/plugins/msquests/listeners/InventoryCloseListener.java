package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.GUIManager;

public class InventoryCloseListener extends MSQuestListener {

    private GUIManager guiManager;

    public InventoryCloseListener(MSQuests plugin, GUIManager guiManager) {
        super(plugin);
        this.guiManager = guiManager;

        register();
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (this.guiManager.inGUI((Player) e.getPlayer())) {
            this.guiManager.setOutOfGUI((Player) e.getPlayer());
        }
    }

}
