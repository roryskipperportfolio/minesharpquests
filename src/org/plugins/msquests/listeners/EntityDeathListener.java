package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.MobsKilledQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class EntityDeathListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public EntityDeathListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onEntityDeath(EntityDeathEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null) {
            QuestType questType = dailyQuest.getQuestType();
            if (questType == QuestType.MOBS_KILLED) {
                Player p = e.getEntity().getKiller();
                if (p != null && !playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                    MobsKilledQuest mkQuest = dailyQuestManager.getMobsKilledQuest();
                    if (mkQuest.getEntityType() == e.getEntityType()) {
                        if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                            dailyQuestManager.completeQuest(p);
                        }
                    }
                }
            }
        }

        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null) {
            QuestType questType = weeklyQuest.getQuestType();
            if (questType == QuestType.MOBS_KILLED) {
                Player p = e.getEntity().getKiller();
                if (p != null && !playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                    MobsKilledQuest mkQuest = weeklyQuestManager.getMobsKilledQuest();
                    if (mkQuest.getEntityType() == e.getEntityType()) {
                        if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                            weeklyQuestManager.completeQuest(p);
                        }
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }
}
