package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.GUIManager;

public class InventoryDragListener extends MSQuestListener {

    private GUIManager guiManager;

    public InventoryDragListener(MSQuests plugin, GUIManager guiManager) {
        super(plugin);
        this.guiManager = guiManager;

        register();
    }

    @EventHandler
    public void onDrag(InventoryDragEvent e) {
        if (this.guiManager.inGUI((Player) e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }

}
