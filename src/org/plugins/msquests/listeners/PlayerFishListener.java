package org.plugins.msquests.listeners;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.FishCaughtQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class PlayerFishListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public PlayerFishListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler (ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onPlayerFish(PlayerFishEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null && dailyQuest.getQuestType() == QuestType.FISH_CAUGHT && e.getState() == PlayerFishEvent.State.CAUGHT_FISH) {
        Player p = e.getPlayer();
        if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                FishCaughtQuest fcQuest = dailyQuestManager.getFishCaughtQuest();
                if (((Item) e.getCaught()).getItemStack().isSimilar(fcQuest.getFishItem())) {
                    if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                        dailyQuestManager.completeQuest(p);
                    }
                }
            }
        }
        
        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null && weeklyQuest.getQuestType() == QuestType.FISH_CAUGHT && e.getState() == PlayerFishEvent.State.CAUGHT_FISH) {
            Player p = e.getPlayer();
            if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                FishCaughtQuest fcQuest = weeklyQuestManager.getFishCaughtQuest();
                if (((Item) e.getCaught()).getItemStack().isSimilar(fcQuest.getFishItem())) {
                    if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                        weeklyQuestManager.completeQuest(p);
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }
}
