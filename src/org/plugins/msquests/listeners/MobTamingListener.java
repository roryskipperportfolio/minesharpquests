package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.MobsTamedQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class MobTamingListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public MobTamingListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onEntityTame(EntityTameEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null && dailyQuest.getQuestType() == QuestType.MOBS_TAMED) {
            Player p = (Player) e.getOwner();
            if (p != null && !playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                MobsTamedQuest mtQuest = dailyQuestManager.getMobsTamedQuest();
                if (mtQuest.getEntityType() == e.getEntityType()) {
                    if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                        dailyQuestManager.completeQuest(p);
                    }
                }
            }
        }

        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null && weeklyQuest.getQuestType() == QuestType.MOBS_TAMED) {
            Player p = (Player) e.getOwner();
            if (p != null && !playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                MobsTamedQuest mtQuest = weeklyQuestManager.getMobsTamedQuest();
                if (mtQuest.getEntityType() == e.getEntityType()) {
                    if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                        weeklyQuestManager.completeQuest(p);
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }
}
