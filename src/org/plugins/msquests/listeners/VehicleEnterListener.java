package org.plugins.msquests.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.MinecartManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class VehicleEnterListener extends MSQuestListener {

    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;
    private final MinecartManager minecartManager;

    public VehicleEnterListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager, MinecartManager minecartManager) {
        super(plugin);
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
        this.minecartManager = minecartManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onVehicleEnter (VehicleEnterEvent e) {
        Quest dailyQuest = this.dailyQuestManager.getCurrentQuest();
        if (dailyQuest.getQuestType() == QuestType.MINECART_TRAVELING) {
            if (e.getVehicle().getType() == EntityType.MINECART && e.getEntered() instanceof Player) {
                Player p = (Player) e.getEntered();
                if (!this.playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !this.playerManager.hasIPCompletedDailyQuest(p)) {
                    if (minecartManager.nobodyInMinecarts()) {
                        this.minecartManager.startTask();
                    }
                    this.minecartManager.updateLastBlock(p, p.getLocation().getBlock());
                    return;
                }
            }
        }
        
        Quest weeklyQuest = this.weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest.getQuestType() == QuestType.MINECART_TRAVELING) {
            if (e.getVehicle().getType() == EntityType.MINECART && e.getEntered() instanceof Player) {
                Player p = (Player) e.getEntered();
                if (!this.playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !this.playerManager.hasIPCompletedWeeklyQuest(p)) {
                    if (minecartManager.nobodyInMinecarts()) {
                        this.minecartManager.startTask();
                    }
                    this.minecartManager.updateLastBlock(p, p.getLocation().getBlock());
                }
            }
        }
    }
}
