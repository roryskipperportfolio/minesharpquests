package org.plugins.msquests.listeners;

import org.bukkit.event.Listener;
import org.plugins.msquests.MSQuests;

public class MSQuestListener implements Listener {

    private final MSQuests plugin;
    boolean isRegistered = false;

    public MSQuestListener(MSQuests plugin) {
        this.plugin = plugin;
    }

    public void register() {
        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
        this.setIsRegistered(true);
    }

    public boolean isRegistered() {
        return this.isRegistered;
    }

    public void setIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

}
