package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.BlocksBrokenQuest;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

public class BlockBreakListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public BlockBreakListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onBreak(BlockBreakEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null && dailyQuest.getQuestType() == QuestType.BLOCKS_BROKEN) {
            Player p = e.getPlayer();
            if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                BlocksBrokenQuest bbQuest = dailyQuestManager.getBlocksBrokenQuest();
                if (bbQuest.getBlockType() == e.getBlock().getType() && bbQuest.getBlockID() == e.getBlock().getData()) {
                    if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                        dailyQuestManager.completeQuest(p);
                    }
                }
            }
        }
        
        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null && weeklyQuest.getQuestType() == QuestType.BLOCKS_BROKEN) {
            Player p = e.getPlayer();
            if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                BlocksBrokenQuest bbQuest = weeklyQuestManager.getBlocksBrokenQuest();
                if (bbQuest.getBlockType() == e.getBlock().getType() && bbQuest.getBlockID() == e.getBlock().getData()) {
                    if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                        weeklyQuestManager.completeQuest(p);
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }
}
