package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.PlayerManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.*;

public class BlockPlaceListener extends MSQuestListener {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;

    public BlockPlaceListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager) {
        super(plugin);
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onPlace(BlockPlaceEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
        Quest dailyQuest = dailyQuestManager.getCurrentQuest();
        if (dailyQuest != null) {
            QuestType questType = dailyQuest.getQuestType();
            if (questType == QuestType.BLOCKS_PLACED) {
                Player p = e.getPlayer();
                if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                    BlocksPlacedQuest bpQuest = dailyQuestManager.getBlocksPlacedQuest();
                    if (bpQuest.getBlockType() == e.getBlock().getType() && bpQuest.getBlockID() == e.getBlock().getData()) {
                        if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                            dailyQuestManager.completeQuest(p);
                        }
                    }
                }
            } else if (questType == QuestType.ITEMS_PLANTED) {
                Player p = e.getPlayer();
                if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                    ItemsPlantedQuest ipQuest = dailyQuestManager.getItemsPlantedQuest();
                    ItemStack item = e.getItemInHand();
                    ItemStack plantItem = ipQuest.getPlantItem();
                    if (item.isSimilar(plantItem)) {
                        if (dailyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.DAILY)) {
                            dailyQuestManager.completeQuest(p);
                        }
                    }
                }
            }
        }

        Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();
        if (weeklyQuest != null) {
            QuestType questType = weeklyQuest.getQuestType();
            if (questType == QuestType.BLOCKS_PLACED) {
                Player p = e.getPlayer();
                if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                    BlocksPlacedQuest bpQuest = weeklyQuestManager.getBlocksPlacedQuest();
                    if (bpQuest.getBlockType() == e.getBlock().getType() && bpQuest.getBlockID() == e.getBlock().getData()) {
                        if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                            weeklyQuestManager.completeQuest(p);
                        }
                    }
                }
            } else if (questType == QuestType.ITEMS_PLANTED) {
                Player p = e.getPlayer();
                if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                    ItemsPlantedQuest ipQuest = weeklyQuestManager.getItemsPlantedQuest();
                    ItemStack item = e.getItemInHand();
                    ItemStack plantItem = ipQuest.getPlantItem();
                    if (item.isSimilar(plantItem)) {
                        if (weeklyQuest.getQuestCompletionNum() == playerManager.incrementProgress(p, QuestTime.WEEKLY)) {
                            weeklyQuestManager.completeQuest(p);
                        }
                    }
                }
            }
        }}
        }.runTaskAsynchronously(this.plugin);
    }

}
