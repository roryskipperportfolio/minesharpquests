package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.MinecartManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestType;

public class VehicleExitListener extends MSQuestListener {

    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final MinecartManager minecartManager;

    public VehicleExitListener(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, MinecartManager minecartManager) {
        super(plugin);
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.minecartManager = minecartManager;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onVehicleExit(VehicleExitEvent e) {
        Quest dailyQuest = this.dailyQuestManager.getCurrentQuest();
        Quest weeklyQuest = this.dailyQuestManager.getCurrentQuest();
        if (dailyQuest.getQuestType() == QuestType.MINECART_TRAVELING || weeklyQuest.getQuestType() == QuestType.MINECART_TRAVELING) {
            if (e.getExited() instanceof Player) {
                Player p = (Player) e.getExited();
                if (this.minecartManager.inMinecart(p)) {
                    this.minecartManager.remove((Player) e.getExited());
                    if (minecartManager.nobodyInMinecarts()) {
                        this.minecartManager.stopTask();
                    }
                }
            }
        }
    }
}
