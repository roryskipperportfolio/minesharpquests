package org.plugins.msquests.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.managers.GUIManager;
import org.plugins.msquests.managers.MinecartManager;
import org.plugins.msquests.managers.PlayerManager;

public class PlayerQuitListener extends MSQuestListener {

    private final MSQuests plugin;
    private final PlayerManager playerManager;
    private final GUIManager guiManager;
    private final MinecartManager minecartManager;

    public PlayerQuitListener(MSQuests plugin, PlayerManager playerManager, GUIManager guiManager, MinecartManager minecartManager) {
        super(plugin);
        this.plugin = plugin;
        this.playerManager = playerManager;
        this.guiManager = guiManager;
        this.minecartManager = minecartManager;

        register();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        this.playerManager.unloadPlayer(p);
        this.guiManager.removeInventories(p);
        this.minecartManager.remove(p);
    }
}
