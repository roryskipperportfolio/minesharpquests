package org.plugins.msquests.managers;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import rorys.library.configs.CustomConfig;
import rorys.library.util.MessagingUtil;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WeeklyQuestManager extends QuestManager {

    private final MSQuests plugin;
    private final MessagingUtil messagingUtil;
    private final PlayerManager playerManager;
    private final CustomConfig questDataConfig;

    private List<Quest> quests = new ArrayList<>();
    private int currentQuestNum, resetDay;
    private Quest currentQuest;
    private Long nextQuestTime;
    private BukkitTask questTimer;

    public WeeklyQuestManager(MSQuests plugin, MessagingUtil messagingUtil, PlayerManager playerManager, CustomConfig questDataConfig) {
        super(plugin);
        this.plugin = plugin;
        this.messagingUtil = messagingUtil;
        this.playerManager = playerManager;
        this.questDataConfig = questDataConfig;

        this.currentQuestNum = this.questDataConfig.getConfig().getInt("weekly-quests.current-quest-num");
        this.loadQuests();
        this.nextQuestTime = this.questDataConfig.getConfig().getLong("weekly-quests.next-quest-time");
    }

    public void loadQuests() {
        this.quests.clear();
        for (String questName : this.plugin.getConfig().getConfigurationSection("weekly-quests").getKeys(false)) {
            this.quests.add(this.loadQuest(this.plugin.getConfig(), "weekly-quests." + questName + ".", questName));
        }
    }

    public void loadCurrentQuest() {
        FileConfiguration config = this.questDataConfig.getConfig();
        if (config.isSet("weekly-quests.current-quest.quest-type")) {
            String path = "weekly-quests.current-quest.";
            this.setCurrentQuest(this.loadQuest(config, path, config.getString(path + "quest-name")));
        } else {
            if (!this.quests.isEmpty()) {
                this.setCurrentQuest(this.quests.get(0));
                this.currentQuestNum = 0;
            }
        }
    }

    public void setCurrentQuest(Quest quest) {
        this.currentQuest = quest;
        this.questDataConfig.getConfig().set("weekly-quests.current-quest", null);
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();

        this.currentQuest.saveQuest(this.questDataConfig, "weekly-quests.current-quest.");

        this.questDataConfig.getConfig().set("weekly-quests.current-quest.quest-name", this.currentQuest.getQuestName());
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();

        this.plugin.registerListeners();
    }

    public Quest getCurrentQuest() {
        return this.currentQuest;
    }

    public void runTimeCheck() {
        if (System.currentTimeMillis() >= this.nextQuestTime) {
            this.updateNextQuestTime();

            if (++this.currentQuestNum == this.quests.size()) {
                this.currentQuestNum = 0;
            }

            this.setCurrentQuest(this.quests.get(this.currentQuestNum));
            this.playerManager.clearPlayers(QuestTime.WEEKLY);
        }
    }

    public Long calculateTimeDifference() {
        long resetTime = (long) (this.plugin.getConfig().getInt("quest-options.weekly-quest-reset-time.time") * 60 * 60);
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
        Date date = new Date(stamp.getTime());
        long hours = date.getHours();
        long mins = date.getMinutes();
        long secs = date.getSeconds();
        Long timeDifference = ((resetTime - secs) - (mins * 60)) - (hours * 60 * 60);
        if (timeDifference <= 0) {
            timeDifference = (24 * 60 * 60) + timeDifference;
            if (this.getDayDifference() == 0) {
                timeDifference += 6 * 24 * 60 * 60;
            }
        }
        timeDifference += (this.getDayDifference() * 24 * 60 * 60);
        return timeDifference * 1000L;
    }

    public void updateNextQuestTime() {
        this.updateNextResetDay();
        long timeDifference = this.calculateTimeDifference();
        this.nextQuestTime = System.currentTimeMillis() + (timeDifference);
        this.questDataConfig.getConfig().set("weekly-quests.next-quest-time", this.nextQuestTime);
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();
        this.stopQuestTimer();
        this.startQuestTimer();
    }

    public void updateNextResetDay() {
        DayOfWeek day = DayOfWeek.valueOf(this.plugin.getConfig().getString("quest-options.weekly-quest-reset-time.day"));
        if (day == DayOfWeek.SUNDAY) {
            this.resetDay = 0;
        } else {
            this.resetDay = day.getValue();
        }
    }

    public void startQuestTimer() {
        this.stopQuestTimer();

        this.questTimer = new BukkitRunnable() {
            @Override
            public void run() {
                runTimeCheck();
            }
        }.runTaskTimerAsynchronously(this.plugin, ((this.nextQuestTime - System.currentTimeMillis()) / 1000L) * 20L + 20L, 7L * 24L * 60L * 60L * 20L);
    }

    public void stopQuestTimer() {
        if (this.questTimer != null) {
            this.questTimer.cancel();
            this.questTimer = null;
        }
    }

    public void completeQuest(Player p) {
        for (String command : this.currentQuest.getQuestCommands()) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYER}", p.getName()));
        }
        this.messagingUtil.sendMessage(p, this.currentQuest.getQuestMessage().replace("{PLAYER}", p.getName()));
        this.playerManager.setCompleted(p, QuestTime.WEEKLY);
    }

    public void fixCurrentQuestNum() {
        if (this.currentQuest != null) {
            int i = -1;
            for (String questName : this.plugin.getConfig().getConfigurationSection("weekly-quests").getKeys(false)) {
                i++;
                if (questName.equals(this.currentQuest.getQuestName())) {
                    this.currentQuestNum = i;
                }
            }
        }
    }

    public void skipQuest() {
        if (++this.currentQuestNum == this.quests.size()) {
            this.currentQuestNum = 0;
        }

        this.setCurrentQuest(this.quests.get(this.currentQuestNum));

        this.playerManager.clearPlayers(QuestTime.WEEKLY);
    }

    public long getNextQuestTime() {
        return this.nextQuestTime;
    }

    public boolean isToday() {
        return getDayDifference() == 0;
    }

    public int getDayDifference() {
        int day = new Date(new Timestamp(System.currentTimeMillis()).getTime()).getDay();
        if (day > this.resetDay) {
            return ((this.resetDay + 7) - day) - 1;
        } else if (day == this.resetDay) {
            return 0;
        } else {
            return (this.resetDay - day) - 1;
        }
    }

    public int getResetDay() {
        return this.resetDay;
    }

}
