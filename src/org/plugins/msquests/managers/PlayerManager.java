package org.plugins.msquests.managers;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.players.QuestProgress;
import org.plugins.msquests.players.Quester;
import org.plugins.msquests.quests.QuestTime;
import rorys.library.configs.CustomConfig;
import rorys.library.configs.PlayerConfigs;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class PlayerManager {

    private final MSQuests plugin;
    private final PlayerConfigs playerConfigs;
    private final CustomConfig questDataConfig;
    private final HashMap<UUID, Quester> questers = new HashMap<>();
    private HashSet<String> dailyCompletedIPS = new HashSet<>();
    private HashSet<String> weeklyCompletedIPS = new HashSet<>();


    public PlayerManager(MSQuests plugin, PlayerConfigs playerConfigs, CustomConfig questDataConfig) {
        this.plugin = plugin;
        this.playerConfigs = playerConfigs;
        this.questDataConfig = questDataConfig;

        this.loadCompletedIPS();
    }

    public Quester loadPlayer(Player p) {
        return this.loadPlayer(p.getUniqueId());
    }

    public Quester loadPlayer(UUID uuid) {
        FileConfiguration config = this.playerConfigs.getConfig(uuid);

        int dailyQuestProgress = config.getInt("daily-quests.quest-progress", 0);
        int dailyTotalQuestsCompleted = config.getInt("daily-quests.total-quests-completed", 0);
        boolean dailyQuestCompleted = config.getBoolean("daily-quests.quest-completed", false);
        QuestProgress dailyProgress = new QuestProgress(dailyQuestProgress, dailyQuestCompleted, dailyTotalQuestsCompleted);

        int weeklyQuestProgress = config.getInt("weekly-quests.quest-progress", 0);
        int weeklyTotalQuestsCompleted = config.getInt("weekly-quests.total-quests-completed", 0);
        boolean weeklyQuestCompleted = config.getBoolean("weekly-quests.quest-completed", false);
        QuestProgress weeklyProgress = new QuestProgress(weeklyQuestProgress, weeklyQuestCompleted, weeklyTotalQuestsCompleted);

        Quester quester = new Quester(dailyProgress, weeklyProgress);
        this.questers.put(uuid, quester);
        return quester;
    }

    public QuestProgress getQuestProgress(Player p, QuestTime questTime) {
        if (questTime == QuestTime.DAILY) {
            return this.getDailyProgress(p.getUniqueId());
        } else if (questTime == QuestTime.WEEKLY) {
            return this.getWeeklyProgress(p.getUniqueId());
        }
        return null;
    }

    private QuestProgress getDailyProgress(UUID uuid) {
        return this.questers.containsKey(uuid) ? this.questers.get(uuid).getDailyQuestProgress() : null;
    }

    private QuestProgress getWeeklyProgress(UUID uuid) {
        return this.questers.containsKey(uuid) ? this.questers.get(uuid).getWeeklyQuestProgress() : null;
    }

    public boolean hasIPCompletedDailyQuest(Player p) {
        return this.dailyCompletedIPS.contains(p.getAddress().getHostName());
    }

    public boolean hasIPCompletedWeeklyQuest(Player p) {
        return this.weeklyCompletedIPS.contains(p.getAddress().getHostName());
    }


    public void setCompleted(Player p, QuestTime questTime) {
        this.questers.get(p.getUniqueId()).getDailyQuestProgress().completeQuest();
        String IP = p.getAddress().getHostName();
        if (questTime == QuestTime.DAILY) {
            this.dailyCompletedIPS.add(IP);
        } else if (questTime == QuestTime.WEEKLY) {
            this.weeklyCompletedIPS.add(IP);
        }
        IP = IP.replace(".", "-");
        this.questDataConfig.getConfig().set(questTime.name().toLowerCase() + "-quests.completed-ips." + IP, p.getName());
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();
    }

    public void loadCompletedIPS() {
        for (String ip : this.questDataConfig.getConfig().getConfigurationSection("daily-quests.completed-ips").getKeys(false)) {
            this.dailyCompletedIPS.add(ip.replace(",", "."));
        }

        for (String ip : this.questDataConfig.getConfig().getConfigurationSection("weekly-quests.completed-ips").getKeys(false)) {
            this.weeklyCompletedIPS.add(ip.replace(",", "."));
        }
    }

    public int incrementProgress(Player p, QuestTime questTime) {
        return this.incrementProgress(p.getUniqueId(), questTime);
    }

    public int incrementProgress(UUID uuid, QuestTime questTime) {
        if (!this.questers.containsKey(uuid)) {
            this.questers.put(uuid, new Quester());
        }

        QuestProgress questProgress;
        if (questTime == QuestTime.DAILY) {
            questProgress = this.questers.get(uuid).getDailyQuestProgress();
        } else if (questTime == QuestTime.WEEKLY) {
            questProgress = this.questers.get(uuid).getWeeklyQuestProgress();
        } else {
            return 0;
        }
        questProgress.incrementQuestProgress();
        return questProgress.getQuestProgress();
    }

    public void unloadPlayer(Player p) {
        this.unloadPlayer(p.getUniqueId());
    }

    public void unloadPlayer(UUID uuid) {
        this.saveProgress(uuid);
        this.questers.remove(uuid);
    }

    private void saveProgress(UUID uuid) {
        Quester quester = this.questers.get(uuid);

        QuestProgress dailyProgress = quester.getDailyQuestProgress();
        this.playerConfigs.getConfig(uuid).set("daily-quests.quest-progress", dailyProgress.getQuestProgress());
        this.playerConfigs.getConfig(uuid).set("daily-quests.quest-completed", dailyProgress.hasCompletedQuest());
        this.playerConfigs.getConfig(uuid).set("daily-quests.total-quests-completed", dailyProgress.getTotalQuestsCompleted());

        QuestProgress weeklyProgress = quester.getWeeklyQuestProgress();
        this.playerConfigs.getConfig(uuid).set("weekly-quests.quest-progress", weeklyProgress.getQuestProgress());
        this.playerConfigs.getConfig(uuid).set("weekly-quests.quest-completed", weeklyProgress.hasCompletedQuest());
        this.playerConfigs.getConfig(uuid).set("weekly-quests.total-quests-completed", weeklyProgress.getTotalQuestsCompleted());

        this.playerConfigs.saveConfig(uuid);
        this.playerConfigs.reloadConfig(uuid);
    }

    public void clearPlayers(QuestTime questTime) {

        this.questers.clear();

        File playerDataFolder = new File(this.plugin.getDataFolder().getPath() + File.separator + "playerdata");
        for (File file : playerDataFolder.listFiles()) {
            UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));
            FileConfiguration config = this.playerConfigs.getConfig(uuid);
            if (questTime == QuestTime.DAILY) {
                config.set("daily-quests.quest-progress", 0);
                config.set("daily-quests.quest-completed", false);
            } else if (questTime == QuestTime.WEEKLY) {
                config.set("weekly-quests.quest-progress", 0);
                config.set("weekly-quests.quest-completed", false);
            }
            this.playerConfigs.saveConfig(uuid);
            this.playerConfigs.reloadConfig(uuid);
        }

        if (questTime == QuestTime.DAILY) {
            this.questDataConfig.getConfig().set("daily-quests.completed-ips", null);
            this.dailyCompletedIPS.clear();
        } else if (questTime == QuestTime.WEEKLY) {
            this.questDataConfig.getConfig().set("weekly-quests.completed-ips", null);
            this.weeklyCompletedIPS.clear();
        }
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();

        for (Player p : Bukkit.getOnlinePlayers()) {
            this.loadPlayer(p);
        }
    }

}
