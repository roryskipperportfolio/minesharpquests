package org.plugins.msquests.managers;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.listeners.*;
import org.plugins.msquests.quests.*;

import java.util.List;

public class QuestManager {

    private final MSQuests plugin;

    private BlocksBrokenQuest blocksBrokenQuest;
    private BlocksPlacedQuest blocksPlacedQuest;
    private FishCaughtQuest fishCaughtQuest;
    private ItemsCraftedQuest itemsCraftedQuest;
    private ItemsPlantedQuest itemsPlantedQuest;
    private ItemsSmeltedQuest itemsSmeltedQuest;
    private MinecartTravelingQuest minecartTravelingQuest;
    private MobsKilledQuest mobsKilledQuest;
    private MobsTamedQuest mobsTamedQuest;

    public QuestManager(MSQuests plugin) {
        this.plugin = plugin;
    }

    public Quest loadQuest(FileConfiguration config, String path, String questName) {
        if (!path.endsWith(".")) {
            path += ".";
        }

        QuestType questType = QuestType.valueOf(config.getString(path + "quest-type"));
        int questCompletionNum = config.getInt(path + "quest-completion");
        String questMessage = config.getString(path + "quest-message");
        List<String> questCommands = config.getStringList(path + "quest-commands");
        List<String> progressBarLore = config.getStringList(path + "progress-bar-lore");
        List<String> questItemLore = config.getStringList(path + "quest-item-lore");

        if (questType == QuestType.BLOCKS_PLACED) {
            Material blockType = Material.valueOf(config.getString(path + "block-type", "AIR"));
            byte blockId = (byte) config.getInt(path + "block-id", 0);
            return new BlocksPlacedQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, blockType, blockId);

        } else if (questType == QuestType.BLOCKS_BROKEN) {
            Material blockType = Material.valueOf(config.getString(path + "block-type", "AIR"));
            byte blockId = (byte) config.getInt(path + "block-id", 0);
            return new BlocksBrokenQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, blockType, blockId);

        } else if (questType == QuestType.ITEMS_PLANTED) {
            Material itemType = Material.valueOf(config.getString(path + "item-type", "AIR"));
            byte itemId = (byte) config.getInt(path + "item-id", 0);
            return new ItemsPlantedQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, itemType, itemId);

        } else if (questType == QuestType.FISH_CAUGHT) {
            Material itemType = Material.valueOf(config.getString(path + "item-type", "AIR"));
            byte itemId = (byte) config.getInt(path + "item-id", 0);
            return new FishCaughtQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, itemType, itemId);

        } else if (questType == QuestType.MOBS_KILLED) {
            EntityType entityType = EntityType.valueOf(config.getString(path + "mob-type"));
            return new MobsKilledQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, entityType);

        } else if (questType == QuestType.MOBS_TAMED) {
            EntityType entityType = EntityType.valueOf(config.getString(path + "mob-type"));
            return new MobsTamedQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, entityType);

        } else if (questType == QuestType.MINECART_TRAVELING) {
            return new MinecartTravelingQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore);

        } else if (questType == QuestType.ITEMS_CRAFTED) {
            Material itemType = Material.valueOf(config.getString(path + "item-type", "AIR"));
            byte itemId = (byte) config.getInt(path + "item-id", 0);
            return new ItemsCraftedQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, itemType, itemId);

        } else if (questType == QuestType.ITEMS_SMELTED) {
            Material itemType = Material.valueOf(config.getString(path + "item-type", "AIR"));
            byte itemId = (byte) config.getInt(path + "item-id", 0);
            return new ItemsSmeltedQuest(questName, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore, itemType, itemId);
        }

        return null;
    }

    public void registerQuest(Quest quest) {
        QuestType questType = quest.getQuestType();
        if (questType == QuestType.BLOCKS_BROKEN) {
            BlockBreakListener blockBreakListener = this.plugin.getBlockBreakListener();
            if (!blockBreakListener.isRegistered()) {
                blockBreakListener.register();
            }
            this.setBlocksBrokenQuest((BlocksBrokenQuest) quest);
        } else if (questType == QuestType.BLOCKS_PLACED) {
            BlockPlaceListener blockPlaceListener = this.plugin.getBlockPlaceListener();
            if (!blockPlaceListener.isRegistered()) {
                blockPlaceListener.register();
            }
            this.setBlocksPlacedQuest((BlocksPlacedQuest) quest);
        } else if (questType == QuestType.ITEMS_PLANTED) {
            BlockPlaceListener blockPlaceListener = this.plugin.getBlockPlaceListener();
            if (!blockPlaceListener.isRegistered()) {
                blockPlaceListener.register();
            }
            this.setItemsPlantedQuest((ItemsPlantedQuest) quest);
        } else if (questType == QuestType.FISH_CAUGHT) {
            PlayerFishListener playerFishListener = this.plugin.getPlayerFishListener();
            if (!playerFishListener.isRegistered()) {
                playerFishListener.register();
            }
            this.setFishCaughtQuest((FishCaughtQuest) quest);
        } else if (questType == QuestType.ITEMS_CRAFTED) {
            ItemCraftListener itemCraftListener = this.plugin.getItemCraftListener();
            if (!itemCraftListener.isRegistered()) {
                itemCraftListener.register();
            }
            this.setItemsCraftedQuest((ItemsCraftedQuest) quest);
        } else if (questType == QuestType.ITEMS_SMELTED) {
            ItemSmeltingListener itemSmeltingListener = this.plugin.getItemSmeltingListener();
            if (!itemSmeltingListener.isRegistered()) {
                itemSmeltingListener.register();
            }
            this.setItemsSmeltedQuest((ItemsSmeltedQuest) quest);
        } else if (questType == QuestType.MINECART_TRAVELING) {
            VehicleEnterListener vehicleEnterListener = this.plugin.getVehicleEnterListener();
            if (!vehicleEnterListener.isRegistered()) {
                vehicleEnterListener.register();
            }
            VehicleExitListener vehicleExitListener = this.plugin.getVehicleExitListener();
            if (!vehicleExitListener.isRegistered()) {
                vehicleExitListener.register();
            }
            this.setMinecartTravelingQuest((MinecartTravelingQuest) quest);
        } else if (questType == QuestType.MOBS_KILLED) {
            EntityDeathListener entityDeathListener = this.plugin.getEntityDeathListener();
            if (!entityDeathListener.isRegistered()) {
                entityDeathListener.register();
            }
            this.setMobsKilledQuest((MobsKilledQuest) quest);
        } else if (questType == QuestType.MOBS_TAMED) {
            MobTamingListener mobTamingListener = this.plugin.getMobTamingListener();
            if (!mobTamingListener.isRegistered()) {
                mobTamingListener.register();
            }
            this.setMobsTamedQuest((MobsTamedQuest) quest);
        }
    }

    public BlocksBrokenQuest getBlocksBrokenQuest() {
        return blocksBrokenQuest;
    }

    public void setBlocksBrokenQuest(BlocksBrokenQuest blocksBrokenQuest) {
        this.blocksBrokenQuest = blocksBrokenQuest;
    }

    public BlocksPlacedQuest getBlocksPlacedQuest() {
        return blocksPlacedQuest;
    }

    public void setBlocksPlacedQuest(BlocksPlacedQuest blocksPlacedQuest) {
        this.blocksPlacedQuest = blocksPlacedQuest;
    }

    public FishCaughtQuest getFishCaughtQuest() {
        return fishCaughtQuest;
    }

    public void setFishCaughtQuest(FishCaughtQuest fishCaughtQuest) {
        this.fishCaughtQuest = fishCaughtQuest;
    }

    public ItemsCraftedQuest getItemsCraftedQuest() {
        return itemsCraftedQuest;
    }

    public void setItemsCraftedQuest(ItemsCraftedQuest itemsCraftedQuest) {
        this.itemsCraftedQuest = itemsCraftedQuest;
    }

    public ItemsPlantedQuest getItemsPlantedQuest() {
        return itemsPlantedQuest;
    }

    public void setItemsPlantedQuest(ItemsPlantedQuest itemsPlantedQuest) {
        this.itemsPlantedQuest = itemsPlantedQuest;
    }

    public ItemsSmeltedQuest getItemsSmeltedQuest() {
        return itemsSmeltedQuest;
    }

    public void setItemsSmeltedQuest(ItemsSmeltedQuest itemsSmeltedQuest) {
        this.itemsSmeltedQuest = itemsSmeltedQuest;
    }

    public MinecartTravelingQuest getMinecartTravelingQuest() {
        return minecartTravelingQuest;
    }

    public void setMinecartTravelingQuest(MinecartTravelingQuest minecartTravelingQuest) {
        this.minecartTravelingQuest = minecartTravelingQuest;
    }

    public MobsKilledQuest getMobsKilledQuest() {
        return mobsKilledQuest;
    }

    public void setMobsKilledQuest(MobsKilledQuest mobsKilledQuest) {
        this.mobsKilledQuest = mobsKilledQuest;
    }

    public MobsTamedQuest getMobsTamedQuest() {
        return mobsTamedQuest;
    }

    public void setMobsTamedQuest(MobsTamedQuest mobsTamedQuest) {
        this.mobsTamedQuest = mobsTamedQuest;
    }
}
