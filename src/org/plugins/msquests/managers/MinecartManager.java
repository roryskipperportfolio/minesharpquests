package org.plugins.msquests.managers;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import org.plugins.msquests.quests.QuestType;

import java.util.HashMap;

public class MinecartManager {

    private final HashMap<Player, Block> lastBlockMap = new HashMap<>();
    private final MSQuests plugin;
    private final PlayerManager playerManager;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;

    private BukkitTask movementTask;

    public MinecartManager(MSQuests plugin, PlayerManager playerManager, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager) {
        this.plugin = plugin;
        this.playerManager = playerManager;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
    }

    public boolean inMinecart(Player p) {
        return this.lastBlockMap.containsKey(p);
    }

    public void updateLastBlock(Player p, Block block) {
        this.lastBlockMap.put(p, block);
    }

    public void remove(Player p) {
        this.lastBlockMap.remove(p);
    }

    public boolean nobodyInMinecarts() {
        return this.lastBlockMap.isEmpty();
    }

    public HashMap<Player, Block> getLastBlockMap() {
        return this.lastBlockMap;
    }

    public void startTask() {
        if (this.movementTask != null) {
            this.movementTask.cancel();
        }

        this.movementTask = new BukkitRunnable() {
            @Override
            public void run() {
                Quest dailyQuest = dailyQuestManager.getCurrentQuest();
                Quest weeklyQuest = weeklyQuestManager.getCurrentQuest();

                boolean stopTask = true;

                if (dailyQuest.getQuestType() == QuestType.MINECART_TRAVELING) {
                    stopTask = false;
                    for (Player p : getLastBlockMap().keySet()) {
                        if (!playerManager.getQuestProgress(p, QuestTime.DAILY).hasCompletedQuest() && !playerManager.hasIPCompletedDailyQuest(p)) {
                            if (!p.getLocation().getBlock().equals(getLastBlockMap().get(p))) {
                                updateLastBlock(p, p.getLocation().getBlock());
                                if (playerManager.incrementProgress(p, QuestTime.DAILY) == dailyQuest.getQuestCompletionNum()) {
                                    dailyQuestManager.completeQuest(p);
                                }
                            }
                        }
                    }
                }

                if (weeklyQuest.getQuestType() == QuestType.MINECART_TRAVELING) {
                    stopTask = false;
                    for (Player p : getLastBlockMap().keySet()) {
                        if (!playerManager.getQuestProgress(p, QuestTime.WEEKLY).hasCompletedQuest() && !playerManager.hasIPCompletedWeeklyQuest(p)) {
                            if (!p.getLocation().getBlock().equals(getLastBlockMap().get(p))) {
                                updateLastBlock(p, p.getLocation().getBlock());
                                if (playerManager.incrementProgress(p, QuestTime.WEEKLY) == weeklyQuest.getQuestCompletionNum()) {
                                    weeklyQuestManager.completeQuest(p);
                                }
                            }
                        }
                    }
                }

                if (stopTask) {
                    stopTask();
                }

            }
        }.runTaskTimer(this.plugin, 20L, 2L);
    }

    public void stopTask() {
        if (this.movementTask != null) {
            this.movementTask.cancel();
            this.movementTask = null;
            this.lastBlockMap.clear();
        }
    }

}
