package org.plugins.msquests.managers;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.gui.GUILocation;
import org.plugins.msquests.players.QuestProgress;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import rorys.library.util.ItemUtil;
import rorys.library.util.MessagingUtil;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class GUIManager {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final PlayerManager playerManager;
    private final MessagingUtil messagingUtil;
    private final ItemUtil itemUtil;
    private final HashMap<UUID, Inventory> mainInventories = new HashMap<>(), dailyInventories = new HashMap<>(), weeklyInventories = new HashMap<>();

    private ItemStack dailyQuestItem, weeklyQuestItem, questStatsItem, backItem;
    private HashMap<Character, ItemStack> redProgressLetters = new HashMap<>();
    private HashMap<Character, ItemStack> greenProgressLetters = new HashMap<>();
    private int mainDailyQuestItemSlot, mainWeeklyQuestItemSlot, dailyQuestItemSlot, weeklyQuestItemSlot, questStatsItemSlot, dailyProgressBarCord, weeklyProgressBarCord, backSlot;
    private Inventory mainInventory, dailyInventory, weeklyInventory;
    private HashMap<UUID, GUILocation> inGUI = new HashMap<>();

    public GUIManager(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, PlayerManager playerManager, MessagingUtil messagingUtil, ItemUtil itemUtil) {
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.playerManager = playerManager;
        this.messagingUtil = messagingUtil;
        this.itemUtil = itemUtil;

        this.updateDailyQuestItem();
        this.updateWeeklyQuestItem();
        this.updateQuestStatsItem();
        this.updateBackItem();
        this.updateSlots();

        this.setupProgressLetters();
        this.setupMainGUI();
        this.setupDailyGUI();
        this.setupWeeklyGUI();
    }

    public void updateDailyQuestItem() {
        this.dailyQuestItem = this.itemUtil.getItemStack("daily-gui-options.quest-item-options");
    }

    public void updateWeeklyQuestItem() {
        this.weeklyQuestItem = this.itemUtil.getItemStack("weekly-gui-options.quest-item-options");
    }

    public void updateQuestStatsItem() {
        this.questStatsItem = this.itemUtil.getItemStack("main-gui-options.quest-stats-item");
    }

    public void updateBackItem() {
        this.backItem = this.itemUtil.getItemStack("main-gui-options.back-item");
    }

    public void updateSlots() {
        this.mainDailyQuestItemSlot = this.itemUtil.getSlot("main-gui-options.daily-quest-item");
        this.mainWeeklyQuestItemSlot = this.itemUtil.getSlot("main-gui-options.weekly-quest-item");
        this.dailyQuestItemSlot = this.itemUtil.getSlot("daily-gui-options.quest-item-options");
        this.weeklyQuestItemSlot = this.itemUtil.getSlot("weekly-gui-options.quest-item-options");
        this.questStatsItemSlot = this.itemUtil.getSlot("main-gui-options.quest-stats-item");
        this.dailyProgressBarCord = this.itemUtil.getSlot("daily-gui-options.progress-bar-options");
        this.weeklyProgressBarCord = this.itemUtil.getSlot("weekly-gui-options.progress-bar-options");
        this.backSlot = this.itemUtil.getSlot("main-gui-options.back-item");
    }

    public int getMainDailyQuestItemSlot() {
        return this.mainDailyQuestItemSlot;
    }

    public int getMainWeeklyQuestItemSlot() {
        return this.mainWeeklyQuestItemSlot;
    }
    public int getBackSlot() {
        return this.backSlot;
    }

    public void setupMainGUI() {
        this.mainInventory = Bukkit.createInventory(null, this.plugin.getConfig().getInt("main-gui-options.rows") * 9, this.messagingUtil.placeholders(this.plugin.getConfig().getString("main-gui-options.title")));

        for (String itemName : this.plugin.getConfig().getConfigurationSection("main-gui-options.static-items").getKeys(false)) {
            String path = "main-gui-options.static-items." + itemName;
            this.mainInventory.setItem(this.itemUtil.getSlot(path), this.itemUtil.getItemStack(path));
        }

        this.mainInventory.setItem(this.questStatsItemSlot, this.questStatsItem);
        this.mainInventory.setItem(this.itemUtil.getSlot("main-gui-options.daily-quest-item"), this.itemUtil.getItemStack("main-gui-options.daily-quest-item"));
        this.mainInventory.setItem(this.itemUtil.getSlot("main-gui-options.weekly-quest-item"), this.itemUtil.getItemStack("main-gui-options.weekly-quest-item"));
    }

    public void setupDailyGUI() {
        this.dailyInventory = Bukkit.createInventory(null, this.plugin.getConfig().getInt("daily-gui-options.rows") * 9, this.messagingUtil.placeholders(this.plugin.getConfig().getString("daily-gui-options.title")));

        for (String itemName : this.plugin.getConfig().getConfigurationSection("daily-gui-options.static-items").getKeys(false)) {
            String path = "daily-gui-options.static-items." + itemName;
            this.dailyInventory.setItem(this.itemUtil.getSlot(path), this.itemUtil.getItemStack(path));
        }

        this.dailyInventory.setItem(this.backSlot, this.backItem);
    }

    public void setupWeeklyGUI() {
        this.weeklyInventory = Bukkit.createInventory(null, this.plugin.getConfig().getInt("weekly-gui-options.rows") * 9, this.messagingUtil.placeholders(this.plugin.getConfig().getString("weekly-gui-options.title")));

        for (String itemName : this.plugin.getConfig().getConfigurationSection("weekly-gui-options.static-items").getKeys(false)) {
            String path = "weekly-gui-options.static-items." + itemName;
            this.weeklyInventory.setItem(this.itemUtil.getSlot(path), this.itemUtil.getItemStack(path));
        }

        this.weeklyInventory.setItem(this.backSlot, this.backItem);
    }

    public void cloneGUI(Player p, GUILocation guiLocation) {
        switch (guiLocation) {
            case MAIN:
                this.cloneMainGUI(p);
            case DAILY:
                this.cloneDailyGUI(p);
            case WEEKLY:
                this.cloneWeeklyGUI(p);
        }
    }

    public Inventory cloneMainGUI(Player p) {
        this.mainInventories.remove(p.getUniqueId());
        Inventory mainInventory = Bukkit.createInventory(p, this.mainInventory.getSize(), this.mainInventory.getTitle());

        for (int i = 0; i < this.mainInventory.getSize(); i++) {
            mainInventory.setItem(i, this.mainInventory.getItem(i));
        }

        this.mainInventories.put(p.getUniqueId(), mainInventory);
        return mainInventory;
    }

    public Inventory cloneDailyGUI(Player p) {
        this.dailyInventories.remove(p.getUniqueId());
        Inventory dailyInventory = Bukkit.createInventory(p, this.dailyInventory.getSize(), this.dailyInventory.getTitle());

        for (int i = 0; i < this.dailyInventory.getSize(); i++) {
            dailyInventory.setItem(i, this.dailyInventory.getItem(i));
        }

        this.dailyInventories.put(p.getUniqueId(), dailyInventory);
        return dailyInventory;
    }

    public Inventory cloneWeeklyGUI(Player p) {
        this.weeklyInventories.remove(p.getUniqueId());
        Inventory weeklyInventory = Bukkit.createInventory(p, this.weeklyInventory.getSize(), this.weeklyInventory.getTitle());

        for (int i = 0; i < this.weeklyInventory.getSize(); i++) {
            weeklyInventory.setItem(i, this.weeklyInventory.getItem(i));
        }

        this.weeklyInventories.put(p.getUniqueId(), weeklyInventory);
        return weeklyInventory;
    }

    public Inventory getDailyInventory(Player p) {
        return this.updateDailyGUI(p);
    }

    public Inventory updateDailyGUI(Player p) {

        Inventory dailyInventory = this.dailyInventories.containsKey(p.getUniqueId()) ? this.dailyInventories.remove(p.getUniqueId()) : this.cloneDailyGUI(p);

        Quest currentQuest = this.dailyQuestManager.getCurrentQuest();

    /*
        {QUEST_COMPLETION} - The amount needed to complete quest
        {QUEST_PROGRESS} - The current progress in the quest (as an integer)
        {QUEST_PERCENTAGE} - The percentage of the quest progress completed
        {PLAYER} - The name of the quester
    */

        QuestProgress dailyProgress = this.playerManager.getQuestProgress(p, QuestTime.DAILY);
        int questCompletionNum = currentQuest.getQuestCompletionNum();
        int questProgress = dailyProgress.getQuestProgress();
        double questPercentage = (double) questProgress / (double) questCompletionNum * 100;
        String questPercentageStr = new DecimalFormat("##0.#").format(questPercentage) + "%";
        String name = p.getName();

        ItemStack questItemClone = this.dailyQuestItem.clone();

        List<String> questItemLore = new ArrayList<>();
        for (String loreLine : this.plugin.getConfig().getStringList("daily-quests." + currentQuest.getQuestName() + ".quest-item-lore")) {
            questItemLore.add(this.messagingUtil.placeholders(loreLine.replace("{QUEST_COMPLETION}", "" + questCompletionNum).replace("{QUEST_PROGRESS}", "" + questProgress).replace("{QUEST_PERCENTAGE}", questPercentageStr).replace("{PLAYER}", name)));
        }

        ItemMeta itemMeta = questItemClone.getItemMeta();
        itemMeta.setLore(questItemLore);
        questItemClone.setItemMeta(itemMeta);
        dailyInventory.setItem(this.dailyQuestItemSlot, questItemClone);

        this.addProgressBar(p, dailyInventory, questPercentage, questCompletionNum, questProgress, questPercentageStr, GUILocation.DAILY);

        this.dailyInventories.put(p.getUniqueId(), dailyInventory);
        return dailyInventory;
    }

    public Inventory getWeeklyInventory(Player p) {
        return this.updateWeeklyGUI(p);
    }

    public Inventory updateWeeklyGUI(Player p) {

        Inventory weeklyInventory = this.weeklyInventories.containsKey(p.getUniqueId()) ? this.weeklyInventories.remove(p.getUniqueId()) : this.cloneWeeklyGUI(p);

        Quest currentQuest = this.weeklyQuestManager.getCurrentQuest();

    /*
        {QUEST_COMPLETION} - The amount needed to complete quest
        {QUEST_PROGRESS} - The current progress in the quest (as an integer)
        {QUEST_PERCENTAGE} - The percentage of the quest progress completed
        {PLAYER} - The name of the quester
    */

        QuestProgress weeklyProgress = this.playerManager.getQuestProgress(p, QuestTime.WEEKLY);
        int questCompletionNum = currentQuest.getQuestCompletionNum();
        int questProgress = weeklyProgress.getQuestProgress();
        double questPercentage = (double) questProgress / (double) questCompletionNum * 100;
        String questPercentageStr = new DecimalFormat("##0.#").format(questPercentage) + "%";
        String name = p.getName();

        ItemStack questItemClone = this.weeklyQuestItem.clone();

        List<String> questItemLore = new ArrayList<>();
        for (String loreLine : this.plugin.getConfig().getStringList("weekly-quests." + currentQuest.getQuestName() + ".quest-item-lore")) {
            questItemLore.add(this.messagingUtil.placeholders(loreLine.replace("{QUEST_COMPLETION}", "" + questCompletionNum).replace("{QUEST_PROGRESS}", "" + questProgress).replace("{QUEST_PERCENTAGE}", questPercentageStr).replace("{PLAYER}", name)));
        }

        ItemMeta itemMeta = questItemClone.getItemMeta();
        itemMeta.setLore(questItemLore);
        questItemClone.setItemMeta(itemMeta);
        weeklyInventory.setItem(this.weeklyQuestItemSlot, questItemClone);

        this.addProgressBar(p, weeklyInventory, questPercentage, questCompletionNum, questProgress, questPercentageStr, GUILocation.WEEKLY);

        this.weeklyInventories.put(p.getUniqueId(), weeklyInventory);
        return weeklyInventory;
    }

    public Inventory getMainInventory(Player p) {
        return this.updateMainGUI(p);
    }

    public Inventory updateMainGUI(Player p) {

        Inventory mainInventory = this.mainInventories.containsKey(p.getUniqueId()) ? this.mainInventories.remove(p.getUniqueId()) : this.cloneMainGUI(p);

    /*
        {TOTAL_DAILY_QUESTS_COMPLETED} - The total daily quests completed
        {TOTAL_WEEKLY_QUESTS_COMPLETED} - The total weekly quests completed
        {TOTAL_QUESTS_COMPLETED} - The total quests completed
    */

        QuestProgress dailyProgress = this.playerManager.getQuestProgress(p, QuestTime.DAILY);
        QuestProgress weeklyProgress = this.playerManager.getQuestProgress(p, QuestTime.WEEKLY);
        int dailyQuestsCompleted = dailyProgress.getTotalQuestsCompleted(), weeklyQuestsCompleted = weeklyProgress.getTotalQuestsCompleted(), totalQuestsCompleted = dailyQuestsCompleted + weeklyQuestsCompleted;
        String name = p.getName();

        ItemStack questStatsItemClone = this.questStatsItem.clone();
        ItemMeta itemMeta = questStatsItemClone.getItemMeta();

        List<String> questItemLore = new ArrayList<>();
        for (String loreLine : itemMeta.getLore()) {
            questItemLore.add(this.messagingUtil.placeholders(loreLine.replace("{DAILY-QUESTS-COMPLETED}", "" + dailyQuestsCompleted).replace("{WEEKLY-QUESTS-COMPLETED}", "" + weeklyQuestsCompleted).replace("{TOTAL-QUESTS-COMPLETED}", "" + totalQuestsCompleted).replace("{PLAYER}", name)));
        }

        itemMeta.setLore(questItemLore);
        questStatsItemClone.setItemMeta(itemMeta);
        mainInventory.setItem(this.questStatsItemSlot, questStatsItemClone);

        this.mainInventories.put(p.getUniqueId(), mainInventory);
        return mainInventory;
    }

    public void removeInventories(Player p) {
        this.mainInventories.remove(p.getUniqueId());
        this.dailyInventories.remove(p.getUniqueId());
        this.weeklyInventories.remove(p.getUniqueId());
    }

    public void addProgressBar(Player p, Inventory inventory, double questPercentage, int questCompletionNum, int questProgress, String questPercentageStr, GUILocation guiLocation) {
        Quest currentQuest = this.dailyQuestManager.getCurrentQuest();
        if (guiLocation == GUILocation.WEEKLY) {
            currentQuest = this.weeklyQuestManager.getCurrentQuest();
        }

        String progressBarName = this.messagingUtil.placeholders(this.plugin.getConfig().getString(guiLocation.name().toLowerCase() + "-gui-options.progress-bar-options.name"));
        List<String> progressBarLore = new ArrayList<>();
        for (String loreLine : this.plugin.getConfig().getStringList(guiLocation.name().toLowerCase() + "-quests." + currentQuest.getQuestName() + ".progress-bar-lore")) {
            progressBarLore.add(this.messagingUtil.placeholders(loreLine.replace("{QUEST_COMPLETION}", "" + questCompletionNum).replace("{QUEST_PROGRESS}", "" + questProgress).replace("{QUEST_PERCENTAGE}", questPercentageStr).replace("{PLAYER}", p.getName())));
        }

        int progressBarCord;
        if (guiLocation == GUILocation.DAILY) {
            progressBarCord = this.dailyProgressBarCord;
        } else {
            progressBarCord = this.weeklyProgressBarCord;
        }

        if (questPercentage < 12.5) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('P'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 2) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('R'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 3) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('O'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 4) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('G'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 5) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('R'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 6) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('E'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 7) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('S'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage < 12.5 * 8) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.redProgressLetters.get('S'), progressBarName, progressBarLore));
        }

        progressBarCord -= 7;

        if (questPercentage >= 12.5) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('P'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 2) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('R'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 3) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('O'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 4) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('G'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 5) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('R'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 6) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('E'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 7) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('S'), progressBarName, progressBarLore));
        }

        progressBarCord++;
        if (questPercentage >= 12.5 * 8) {
            inventory.setItem(progressBarCord, this.addNameAndLore(this.greenProgressLetters.get('S'), progressBarName, progressBarLore));
        }

    }

    public void setupProgressLetters() {
        this.redProgressLetters.put('P', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTY1ZjIzZGRjNmU4M2FiN2QwYzRhYTljNTc0NGFmN2I5NmJjNzM5YmM4M2E5NmNiMWYyYjE4ZDY3MWYifX19\\"));
        this.redProgressLetters.put('R', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzVmNDVjODc1ZWZmNTM4ZmNlYzk4ZjZhY2MxZGYyYWVjYWUyOGY0ODYwYWVjZDI0ZTJkYmRmMTM5MjRiMzI3In19fQ==\\"));
        this.redProgressLetters.put('O', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmFlY2VkZjViOGIwYWMzZjU2MTkyZmNjYjBhZGU0OGRjNWEwNDNlN2UwZWVhMmJjYzVmNTRhZDAyODFmNjdjOSJ9fX0=\\"));
        this.redProgressLetters.put('G', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTg1MzYxMzc3MjE1ZTIxNGEzNWM5NmNiYWY5ZGI2NzIxMDVhZmFmYzk0OTRiYmY0YWU3MmFhNjczYzVmZTdjIn19fQ==\\"));
        this.redProgressLetters.put('E', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmY0ZjI1MTc1MmM3MzE3YmIyZmI2MjhlNGMzN2M4MmM2MDcxOWQ5MDk5ODUxNDZiMjYxODMyMTUyYTMwYWRhMiJ9fX0=\\"));
        this.redProgressLetters.put('S', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmRkMmNjZTA0Njc0YzJjM2Q1YTNhOTRmZjIxOTc4N2EyYjQ1OWRlNzkwYTBjMDFmZjI5Yjk2NzI5MDcyY2QifX19\\"));

        this.greenProgressLetters.put('P', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGY1YjQwOTQxN2ZmMmU3NzdkNWU4M2NlY2QyODU2ZDJjY2M2OGRmZmRlZjk0MDE1NGU5NDVhY2U2ZDljY2MifX19\\"));
        this.greenProgressLetters.put('R', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzkxZDQ2ZjE4NDQzNDFiYzk0ZDFmOGY0NTE5ZDNmN2M2ZTJmYTcxOGI2NjRiOTM1NjYxYTI3ODJhNjcwZTIifX19\\"));
        this.greenProgressLetters.put('O', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZThmNDc5MjM4NjFjMjc1MzExYWI2OWIwM2UyNzUxMjI0ZmYzOTRlNDRlM2IzYzhkYjUyMjVmZiJ9fX0=\\"));
        this.greenProgressLetters.put('G', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTdjYThiOWM3MTY5YTc0M2Y4MmU4ZTlmZDY4ZjExYzBmMzEwNzBhNTNjOTI4NjM4ODRlNjczMTM3ZmJhZjEifX19\\"));
        this.greenProgressLetters.put('E', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTE4NjQxMDE2ZDFkN2NiNDA2YTg2M2RkZTgxZjZiYmQzMDM3MWNmNDZkOGYxNDIwY2Q1ZWM2Y2RiNjRlZjcifX19\\"));
        this.greenProgressLetters.put('S', this.createCustomHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkOWQ5NmMzOTNlMzhhMzZiNjFhYTNjODU5ZWRlNWViNzQ0ZWYxZTg0NmQ0ZjdkMGVjYmQ2NTg4YTAyMSJ9fX0=\\"));
    }

    public ItemStack createCustomHead(String value) {
        String signature = "H116D5fhmj/7BVWqiRQilXmvoJO6wJzXH4Dvou6P2o9YMb+HaJT8s9+zt03GMYTipzK+NsW2D2JfzagnxLUTuiOtrCHm6V2udOM0HG0JeL4zR0Wn5oHmu+S7kUPUbt7HVlKaRXry5bobFQ06nUf7hOV3kPfpUJsfMajfabmoJ9RGMRVot3uQszjKOHQjxyAjfHP2rjeI/SktBrSscx0DzwBW9LCra7g/6Cp7/xPQTIZsqz2Otgp6i2h3YpXJPy02j4pIk0H4biR3CaU7FB0V4/D1Hvjd08giRvUpqF0a1w9rbpIWIH5GTUP8eLFdG/9SnHqMCQrTj4KkQiN0GdBO18JvJS/40LTn3ZLag5LBIa7AyyGus27N3wdIccvToQ6kHHRVpW7cUSXjircg3LOsSQbJmfLoVJ/KAF/m+de4PxIjOJIcbiOkVyQfMQltPg26VzRiu3F0qRvJNAAydH8AHdaqhkpSf6yjHqPU3p3BHFJld5o59WoD4WNkE3wOC//aTpV/f9RJ0JQko08v2mGBVKx7tpN7vHD1qD5ILzV1nDCV1/qbKgiOK9QmdXqZw9J3pM/DHtZ6eiRKni9BuGWlbWFN/qfFO2xY+J7SYFqTxBbffmvwvuF83QP5UdRTNVLYoV5S+yR5ac7fVWUZmLbq7tawyuCu0Dw24M9E1BSnpSc=";

        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        gameProfile.getProperties().put("textures", new Property("textures", value, signature));

        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        Field profileField = null;
        try {
            profileField = skullMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(skullMeta, gameProfile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException exception) {
            exception.printStackTrace();
        }
        skull.setItemMeta(skullMeta);

        return skull;
    }

    public ItemStack addNameAndLore(ItemStack itemStack, String name, List lore) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public void openGUI(Player p, GUILocation guiLocation) {
        if (guiLocation == GUILocation.MAIN) {
            p.openInventory(this.updateMainGUI(p));
        } else if (guiLocation == GUILocation.DAILY) {
            p.openInventory(this.updateDailyGUI(p));
        } else if (guiLocation == GUILocation.WEEKLY) {
            p.openInventory(this.updateWeeklyGUI(p));
        }
        this.inGUI.put(p.getUniqueId(), guiLocation);
    }

    public boolean inGUI(Player p) {
        return this.inGUI.containsKey(p.getUniqueId());
    }

    public boolean inGUI(Player p, GUILocation guiLocation) {
        return this.inGUI.containsKey(p.getUniqueId()) ? this.inGUI.get(p.getUniqueId()) == guiLocation : false;
    }

    public GUILocation getGUILocation(Player p) {
        return this.inGUI.containsKey(p.getUniqueId()) ? this.inGUI.get(p.getUniqueId()) : null;
    }

    public void setOutOfGUI(Player p) {
        this.inGUI.remove(p.getUniqueId());
    }

}
