package org.plugins.msquests.managers;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.quests.Quest;
import org.plugins.msquests.quests.QuestTime;
import rorys.library.configs.CustomConfig;
import rorys.library.util.MessagingUtil;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailyQuestManager extends QuestManager {

    private final MSQuests plugin;
    private final MessagingUtil messagingUtil;
    private final PlayerManager playerManager;
    private final CustomConfig questDataConfig;

    private List<Quest> quests = new ArrayList<>();
    private int currentQuestNum;
    private Quest currentQuest;
    private Long nextQuestTime;
    private BukkitTask questTimer;

    public DailyQuestManager(MSQuests plugin, MessagingUtil messagingUtil, PlayerManager playerManager, CustomConfig questDataConfig) {
        super(plugin);
        this.plugin = plugin;
        this.messagingUtil = messagingUtil;
        this.playerManager = playerManager;
        this.questDataConfig = questDataConfig;

        this.currentQuestNum = this.questDataConfig.getConfig().getInt("daily-quests.current-quest-num");
        this.loadQuests();
        this.nextQuestTime = this.questDataConfig.getConfig().getLong("daily-quests.next-quest-time");
    }

    public void loadQuests() {
        this.quests.clear();
        for (String questName : this.plugin.getConfig().getConfigurationSection("daily-quests").getKeys(false)) {
            this.quests.add(this.loadQuest(this.plugin.getConfig(), "daily-quests." + questName + ".", questName));
        }
    }

    public void loadCurrentQuest() {
        FileConfiguration config = this.questDataConfig.getConfig();
        if (config.isSet("daily-quests.current-quest.quest-type")) {
            String path = "daily-quests.current-quest.";
            this.setCurrentQuest(this.loadQuest(config, path, config.getString(path + "quest-name")));
        } else {
            this.setCurrentQuest(this.quests.get(0));
            this.currentQuestNum = 0;
        }
    }

    public void setCurrentQuest(Quest quest) {
        this.currentQuest = quest;
        this.questDataConfig.getConfig().set("daily-quests.current-quest", null);
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();

        this.currentQuest.saveQuest(this.questDataConfig, "daily-quests.current-quest.");

        this.questDataConfig.getConfig().set("daily-quests.current-quest.quest-name", this.currentQuest.getQuestName());
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();

        this.plugin.registerListeners();
    }

    public Quest getCurrentQuest() {
        return this.currentQuest;
    }

    public void runTimeCheck() {
        if (System.currentTimeMillis() >= this.nextQuestTime) {
            this.updateNextQuestTime();

            if (++this.currentQuestNum == this.quests.size()) {
                this.currentQuestNum = 0;
            }

            this.setCurrentQuest(this.quests.get(this.currentQuestNum));
            this.playerManager.clearPlayers(QuestTime.DAILY);
        }
    }

    public Long calculateTimeDifference() {
        long resetTime = (long) (this.plugin.getConfig().getInt("quest-options.daily-quest-reset-time") * 60 * 60);
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
        Date date = new Date(stamp.getTime());
        long hours = date.getHours();
        long mins = date.getMinutes();
        long secs = date.getSeconds();
        Long timeDifference = resetTime - secs - (mins * 60) - (hours * 60 * 60);
        if (timeDifference <= 0) {
            timeDifference = (24 * 60 * 60) + timeDifference;
        }
        return timeDifference * 1000L;
    }

    public void updateNextQuestTime() {
        long timeDifference = this.calculateTimeDifference();
        this.nextQuestTime = System.currentTimeMillis() + (timeDifference);
        this.questDataConfig.getConfig().set("daily-quests.next-quest-time", this.nextQuestTime);
        this.questDataConfig.saveConfig();
        this.questDataConfig.reloadConfig();
        this.stopQuestTimer();
        this.startQuestTimer();
    }

    public void startQuestTimer() {
        if (this.questTimer != null) {
            this.questTimer.cancel();
        }

        this.questTimer = new BukkitRunnable() {
            @Override
            public void run() {
                runTimeCheck();
            }
        }.runTaskTimer(this.plugin, ((this.nextQuestTime - System.currentTimeMillis()) / 1000L) * 20L + 20L, 24L * 60L * 60L * 20L);
    }

    public void stopQuestTimer() {
        if (this.questTimer != null) {
            this.questTimer.cancel();
            this.questTimer = null;
        }
    }

    public void completeQuest(Player p) {
        for (String command : this.currentQuest.getQuestCommands()) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYER}", p.getName()));
        }
        this.messagingUtil.sendMessage(p, this.currentQuest.getQuestMessage().replace("{PLAYER}", p.getName()));
        this.playerManager.setCompleted(p, QuestTime.DAILY);
    }

    public void fixCurrentQuestNum() {
        if (this.currentQuest != null) {
            int i = -1;
            for (String questName : this.plugin.getConfig().getConfigurationSection("daily-quests").getKeys(false)) {
                i++;
                if (questName.equals(this.currentQuest.getQuestName())) {
                    this.currentQuestNum = i;
                }
            }
        }
    }

    public void skipQuest() {
        if (++this.currentQuestNum == this.quests.size()) {
            this.currentQuestNum = 0;
        }

        this.setCurrentQuest(this.quests.get(this.currentQuestNum));

        this.playerManager.clearPlayers(QuestTime.DAILY);
    }

    public long getNextQuestTime() {
        return this.nextQuestTime;
    }

}
