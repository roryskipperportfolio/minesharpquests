package org.plugins.msquests.players;

public class Quester {

    private QuestProgress dailyProgress, weeklyProgress;

    public Quester() {
        this.dailyProgress = new QuestProgress(0, false, 0);
        this.weeklyProgress = new QuestProgress(0, false, 0);
    }

    public Quester(QuestProgress dailyProgress, QuestProgress weeklyProgress) {
        this.dailyProgress = dailyProgress;
        this.weeklyProgress = weeklyProgress;
    }

    public QuestProgress getDailyQuestProgress() {
        return this.dailyProgress;
    }

    public QuestProgress getWeeklyQuestProgress() {
        return this.weeklyProgress;
    }

}
