package org.plugins.msquests.players;

public class QuestProgress {

        private int questProgress, totalQuestsCompleted;
        private boolean completed;

        public QuestProgress(int questProgress, boolean completed, int totalQuestsCompleted) {
            this.questProgress = questProgress;
            this.completed = completed;
            this.totalQuestsCompleted = totalQuestsCompleted;
        }

        public int getQuestProgress() {
            return this.questProgress;
        }

        public void incrementQuestProgress() {
            this.questProgress++;
        }

        public boolean hasCompletedQuest() {
            return this.completed;
        }

        public void completeQuest() {
            if (!this.completed) {
                this.completed = true;
                this.totalQuestsCompleted++;
            }
        }

        public int getTotalQuestsCompleted() {
            return this.totalQuestsCompleted;
        }
}
