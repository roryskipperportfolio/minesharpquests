package org.plugins.msquests.quests;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import rorys.library.configs.CustomConfig;

import java.util.List;

public class FishCaughtQuest extends Quest {

    private ItemStack fishItem;

    public FishCaughtQuest(String questName, int questCompletionNum, String questMessage, List<String> questCommands, List<String> progressBarLore, List<String> questItemLore, Material itemType, short itemData) {
        super(questName, QuestType.FISH_CAUGHT, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore);
        this.fishItem = new ItemStack(itemType, 1, itemData);
    }

    @Override
    public void saveQuest(CustomConfig customConfig, String path) {
        if (!path.endsWith(".")) {
            path += ".";
        }

        FileConfiguration config = customConfig.getConfig();

        config.set(path + "quest-type", this.questType.toString());
        config.set(path + "item-type", this.fishItem.getType().name());
        config.set(path + "item-id", this.fishItem.getDurability());
        config.set(path + "quest-completion", this.questCompletionNum);
        config.set(path + "quest-message", this.questMessage);
        config.set(path + "quest-commands", this.questCommands);
        config.set(path + "progress-bar-lore", this.progressBarLore);
        config.set(path + "quest-item-lore", this.questItemLore);

        customConfig.saveConfig();
        customConfig.reloadConfig();
    }

    public ItemStack getFishItem() {
        return this.fishItem;
    }
}
