package org.plugins.msquests.quests;

public enum QuestType {

    MOBS_KILLED(),
    BLOCKS_BROKEN(),
    BLOCKS_PLACED(),
    FISH_CAUGHT(),
    ITEMS_CRAFTED(),
    MINECART_TRAVELING(),
    ITEMS_SMELTED(),
    ITEMS_PLANTED(),
    MOBS_TAMED();

}
