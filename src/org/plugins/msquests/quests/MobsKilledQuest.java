package org.plugins.msquests.quests;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import rorys.library.configs.CustomConfig;

import java.util.List;

public class MobsKilledQuest extends Quest {

    private EntityType entityType;

    public MobsKilledQuest(String questName, int questCompletionNum, String questMessage, List<String> questCommands, List<String> progressBarLore, List<String> questItemLore, EntityType entityType) {
        super(questName, QuestType.MOBS_KILLED, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore);
        this.entityType = entityType;
    }

    @Override
    public void saveQuest(CustomConfig customConfig, String path) {
        if (!path.endsWith(".")) {
            path += ".";
        }

        FileConfiguration config = customConfig.getConfig();

        config.set(path + "quest-type", this.questType.toString());
        config.set(path + "mob-type", this.entityType.name());
        config.set(path + "quest-completion", this.questCompletionNum);
        config.set(path + "quest-message", this.questMessage);
        config.set(path + "quest-commands", this.questCommands);
        config.set(path + "progress-bar-lore", this.progressBarLore);
        config.set(path + "quest-item-lore", this.questItemLore);

        customConfig.saveConfig();
        customConfig.reloadConfig();
    }

    public EntityType getEntityType() {
        return this.entityType;
    }
}
