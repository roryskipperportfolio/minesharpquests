package org.plugins.msquests.quests;

import org.bukkit.configuration.file.FileConfiguration;
import rorys.library.configs.CustomConfig;

import java.util.List;

public abstract class Quest {

    String questName;
    QuestType questType;
    int questCompletionNum;
    String questMessage;
    List<String> questCommands, progressBarLore, questItemLore;

    public Quest(String questName, QuestType questType, int questCompletionNum, String questMessage, List<String> questCommands, List<String> progressBarLore, List<String> questItemLore) {
        this.questName = questName;
        this.questType = questType;
        this.questCompletionNum = questCompletionNum;
        this.questMessage = questMessage;
        this.questCommands = questCommands;
        this.progressBarLore = progressBarLore;
        this.questItemLore = questItemLore;
    }

    public String getQuestName() {
        return this.questName;
    }

    public QuestType getQuestType() {
        return this.questType;
    }

    public void setQuestType(QuestType questType) {
        this.questType = questType;
    }

    public int getQuestCompletionNum() {
        return this.questCompletionNum;
    }

    public void setQuestCompletionNum(int questCompletionNum) {
        this.questCompletionNum = questCompletionNum;
    }

    public String getQuestMessage() {
        return this.questMessage;
    }

    public void setQuestMessage(String questMessage) {
        this.questMessage = questMessage;
    }

    public List<String> getQuestCommands() {
        return this.questCommands;
    }

    public void setQuestCommands(List<String> questCommands) {
        this.questCommands = questCommands;
    }

    public List<String> getProgressBarLore() {
        return this.progressBarLore;
    }

    public void setProgressBarLore(List<String> progressBarLore) {
        this.progressBarLore = progressBarLore;
    }

    public List<String> getQuestItemLore() {
        return this.questItemLore;
    }

    public void setQuestItemLore(List<String> questItemLore) {
        this.questItemLore = questItemLore;
    }

    public void saveQuest(CustomConfig customConfig, String path) {
        if (!path.endsWith(".")) {
            path += ".";
        }

        FileConfiguration config = customConfig.getConfig();

        config.set(path + "quest-type", this.questType.toString());
        config.set(path + "quest-completion", this.questCompletionNum);
        config.set(path + "quest-message", this.questMessage);
        config.set(path + "quest-commands", this.questCommands);
        config.set(path + "progress-bar-lore", this.progressBarLore);
        config.set(path + "quest-item-lore", this.questItemLore);

        customConfig.saveConfig();
        customConfig.reloadConfig();
    }
}
