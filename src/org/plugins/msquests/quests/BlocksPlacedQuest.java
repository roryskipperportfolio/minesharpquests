package org.plugins.msquests.quests;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import rorys.library.configs.CustomConfig;

import java.util.List;

public class BlocksPlacedQuest extends Quest {

    private Material blockType;
    private byte blockID;

    public BlocksPlacedQuest(String questName, int questCompletionNum, String questMessage, List<String> questCommands, List<String> progressBarLore, List<String> questItemLore, Material blockType, byte blockID) {
        super(questName, QuestType.BLOCKS_PLACED, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore);
        this.blockType = blockType;
        this.blockID = blockID;
    }

    @Override
    public void saveQuest(CustomConfig customConfig, String path) {
        if (!path.endsWith(".")) {
            path += ".";
        }

        FileConfiguration config = customConfig.getConfig();

        config.set(path + "quest-type", this.questType.toString());
        config.set(path + "block-type", this.blockType.name());
        config.set(path + "block-id", this.blockID);
        config.set(path + "quest-completion", this.questCompletionNum);
        config.set(path + "quest-message", this.questMessage);
        config.set(path + "quest-commands", this.questCommands);
        config.set(path + "progress-bar-lore", this.progressBarLore);
        config.set(path + "quest-item-lore", this.questItemLore);

        customConfig.saveConfig();
        customConfig.reloadConfig();
    }

    public Material getBlockType() {
        return this.blockType;
    }

    public short getBlockID() {
        return this.blockID;
    }
}
