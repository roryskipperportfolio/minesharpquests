package org.plugins.msquests.quests;

import java.util.List;

public class MinecartTravelingQuest extends Quest {

    public MinecartTravelingQuest(String questName, int questCompletionNum, String questMessage, List<String> questCommands, List<String> progressBarLore, List<String> questItemLore) {
        super(questName, QuestType.MINECART_TRAVELING, questCompletionNum, questMessage, questCommands, progressBarLore, questItemLore);
    }
}
