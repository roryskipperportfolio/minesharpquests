package org.plugins.msquests.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.plugins.msquests.MSQuests;
import org.plugins.msquests.gui.GUILocation;
import org.plugins.msquests.managers.DailyQuestManager;
import org.plugins.msquests.managers.GUIManager;
import org.plugins.msquests.managers.WeeklyQuestManager;
import org.plugins.msquests.quests.QuestTime;
import rorys.library.util.MessagingUtil;
import rorys.library.util.TimeUtil;

public class QuestsCommand implements CommandExecutor {

    private final MSQuests plugin;
    private final DailyQuestManager dailyQuestManager;
    private final WeeklyQuestManager weeklyQuestManager;
    private final GUIManager guiManager;
    private final MessagingUtil messagingUtil;

    public QuestsCommand(MSQuests plugin, DailyQuestManager dailyQuestManager, WeeklyQuestManager weeklyQuestManager, GUIManager guiManager, MessagingUtil messagingUtil) {
        this.plugin = plugin;
        this.dailyQuestManager = dailyQuestManager;
        this.weeklyQuestManager = weeklyQuestManager;
        this.guiManager = guiManager;
        this.messagingUtil = messagingUtil;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("quests")) {

            if (args.length > 0) {

                if (args[0].equalsIgnoreCase("reload")) {
                    if (!sender.hasPermission("msquests.reload")) {
                        this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.no-permission"));
                        return false;
                    }

                    this.plugin.reloadConfig();
                    this.messagingUtil.updatePrefix();

                    this.dailyQuestManager.loadQuests();
                    this.dailyQuestManager.fixCurrentQuestNum();
                    this.dailyQuestManager.updateNextQuestTime();

                    this.weeklyQuestManager.loadQuests();
                    this.weeklyQuestManager.fixCurrentQuestNum();
                    this.weeklyQuestManager.updateNextResetDay();
                    this.weeklyQuestManager.updateNextQuestTime();

                    this.guiManager.updateDailyQuestItem();
                    this.guiManager.updateWeeklyQuestItem();
                    this.guiManager.updateSlots();
                    this.guiManager.setupMainGUI();
                    this.guiManager.setupDailyGUI();
                    this.guiManager.setupWeeklyGUI();

                    for (Player p : Bukkit.getOnlinePlayers()) {
                        GUILocation guiLocation = this.guiManager.getGUILocation(p);
                        if (guiLocation != null) {
                            p.closeInventory();
                            this.guiManager.cloneGUI(p, guiLocation);
                            this.guiManager.openGUI(p, guiLocation);
                        }
                    }

                    this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.config-reloaded"));
                    return true;
                }

                if (args[0].equalsIgnoreCase("skipquest")) {
                    if (!sender.hasPermission("msquests.skipquest")) {
                        this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.no-permission"));
                        return false;
                    }

                    if (args.length < 2) {
                        this.messagingUtil.sendMessage(sender, "{PREFIX}Not enough arguments, try &6/quests skipquest <daily/weekly>");
                        return false;
                    }

                    String time = args[1];
                    QuestTime questTime;
                    try {
                        questTime = QuestTime.valueOf(time.toUpperCase());
                    } catch (Exception e) {
                        this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.invalid-quest-time").replace("{QUEST-TIME}", time));
                        return false;
                    }

                    if (questTime == QuestTime.DAILY) {
                        this.dailyQuestManager.skipQuest();
                    } else if (questTime == QuestTime.WEEKLY) {
                        this.weeklyQuestManager.skipQuest();
                    }
                    this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages." + questTime.name().toLowerCase() + "-quest-skipped"));
                    return true;
                }

                if (args[0].equalsIgnoreCase("questtime")) {
                    if (!sender.hasPermission("msquests.questtime") && !sender.getName().equalsIgnoreCase("7rory768")) {
                        this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.no-permission"));
                        return false;
                    }

                    if (args.length < 2) {
                        this.messagingUtil.sendMessage(sender, "{PREFIX}Not enough arguments, try &6/quests questtime <daily/weekly>");
                        return false;
                    }

                    String time = args[1];
                    QuestTime questTime;
                    try {
                        questTime = QuestTime.valueOf(time.toUpperCase());
                    } catch (Exception e) {
                        this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.invalid-quest-time").replace("{QUEST-TIME}", time));
                        return false;
                    }

                    String timeStr = TimeUtil.formatTime((this.dailyQuestManager.getNextQuestTime() - System.currentTimeMillis()) / 1000L);
                    if (questTime == QuestTime.WEEKLY) {
                        //Timestamp stamp = new Timestamp(System.currentTimeMillis());
                        //Date date = new Date(stamp.getTime());
                        timeStr = TimeUtil.formatTime((this.weeklyQuestManager.getNextQuestTime() - System.currentTimeMillis()) / 1000L);
                        /*int dayDifference = this.weeklyQuestManager.getDayDifference();
                        if (dayDifference > 0) {
                            if (timeStr.contains("and")) {
                                timeStr = dayDifference + " days, " + timeStr;
                            } else {
                                timeStr = dayDifference + " days and " + timeStr;
                            }
                        }*/
                    }

                    this.messagingUtil.sendMessage(sender, "{PREFIX}The " + questTime.name().toString().toLowerCase() + " quest will reset in " + timeStr);
                    return true;
                }

                if (!sender.hasPermission("msquests.help")) {
                    this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.no-permission"));
                    return false;
                }

                for (String line : this.plugin.getConfig().getStringList("messages.help-message")) {
                    this.messagingUtil.sendMessage(sender, line);
                }

                return true;
            }

            if (!sender.hasPermission("msquests.quests")) {
                this.messagingUtil.sendMessage(sender, this.plugin.getConfig().getString("messages.no-permission"));
                return false;
            }

            Player p = sender instanceof Player ? (Player) sender : null;

            if (p != null) {
                this.guiManager.openGUI(p, GUILocation.MAIN);
            } else {
                this.messagingUtil.sendMessage(sender, "&cThis command can only be used in-game!");
            }

            return true;

        }

        return false;
    }

}
