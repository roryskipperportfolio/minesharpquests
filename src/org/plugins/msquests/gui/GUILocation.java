package org.plugins.msquests.gui;

public enum GUILocation {

    MAIN(),
    DAILY(),
    WEEKLY();

}
