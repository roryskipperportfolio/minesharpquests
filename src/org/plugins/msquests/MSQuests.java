package org.plugins.msquests;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import org.plugins.msquests.commands.QuestsCommand;
import org.plugins.msquests.listeners.*;
import org.plugins.msquests.managers.*;
import rorys.library.RorysLibrary;
import rorys.library.configs.CustomConfig;
import rorys.library.configs.PlayerConfigs;
import rorys.library.util.CustomConfigUtil;

import java.io.File;
import java.io.IOException;

public class MSQuests extends JavaPlugin {

    private MSQuests plugin;
    private RorysLibrary rorysLibrary;
    private PlayerConfigs playerConfigs;
    private CustomConfig questdataConfig;
    private GUIManager guiManager;
    private PlayerManager playerManager;
    private DailyQuestManager dailyQuestManager;
    private WeeklyQuestManager weeklyQuestManager;
    private MinecartManager minecartManager;

    private BlockBreakListener blockBreakListener;
    private BlockPlaceListener blockPlaceListener;
    private EntityDeathListener entityDeathListener;
    private InventoryClickListener inventoryClickListener;
    private InventoryCloseListener inventoryCloseListener;
    private InventoryDragListener inventoryDragListener;
    private ItemCraftListener itemCraftListener;
    private ItemSmeltingListener itemSmeltingListener;
    private MobTamingListener mobTamingListener;
    private PlayerFishListener playerFishListener;
    private PlayerJoinListener playerJoinListener;
    private PlayerQuitListener playerQuitListener;
    private VehicleEnterListener vehicleEnterListener;
    private VehicleExitListener vehicleExitListener;

    @Override
    public void onEnable() {
        this.plugin = this;
        this.rorysLibrary = new RorysLibrary(this);

        this.setupConfigs();
        this.convertData();
        this.initializeVariables();
        this.setupListeners();
        this.loadCommands();

        for (Player p : Bukkit.getOnlinePlayers()) {
            this.playerManager.loadPlayer(p);
            this.guiManager.cloneMainGUI(p);
            this.guiManager.cloneDailyGUI(p);
            this.guiManager.cloneWeeklyGUI(p);
        }

        this.dailyQuestManager.loadCurrentQuest();
        this.dailyQuestManager.fixCurrentQuestNum();
        this.dailyQuestManager.updateNextQuestTime();
        this.dailyQuestManager.runTimeCheck();

        this.weeklyQuestManager.loadCurrentQuest();
        this.weeklyQuestManager.fixCurrentQuestNum();
        this.weeklyQuestManager.updateNextQuestTime();
        this.weeklyQuestManager.runTimeCheck();
    }

    @Override
    public void onDisable() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (this.guiManager.inGUI(p)) {
                p.closeInventory();
            }

            this.playerManager.unloadPlayer(p);

        }

        this.dailyQuestManager.stopQuestTimer();
        this.minecartManager.stopTask();

    }

    public void initializeVariables() {
        this.playerManager = new PlayerManager(this, this.playerConfigs, this.questdataConfig);
        this.dailyQuestManager = new DailyQuestManager(this.plugin, this.rorysLibrary.getMessagingUtil(), this.playerManager, this.questdataConfig);
        this.weeklyQuestManager = new WeeklyQuestManager(this.plugin, this.rorysLibrary.getMessagingUtil(), this.playerManager, this.questdataConfig);
        this.guiManager = new GUIManager(this.plugin, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager, this.rorysLibrary.getMessagingUtil(), this.rorysLibrary.getItemUtil());
        this.minecartManager = new MinecartManager(this, this.playerManager, this.dailyQuestManager, this.weeklyQuestManager);
    }

    public void setupConfigs() {
        this.playerConfigs = new PlayerConfigs(this);
        this.questdataConfig = new CustomConfig(this, "questdata");

        CustomConfigUtil.loadDefaultConfig(this);
        CustomConfigUtil.loadConfig(this.questdataConfig);
    }

    public void setupListeners() {
        this.blockBreakListener = new BlockBreakListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.blockPlaceListener = new BlockPlaceListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.entityDeathListener = new EntityDeathListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.playerFishListener = new PlayerFishListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.itemCraftListener = new ItemCraftListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.itemSmeltingListener = new ItemSmeltingListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.vehicleExitListener = new VehicleExitListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.minecartManager);
        this.vehicleEnterListener = new VehicleEnterListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager, this.minecartManager);
        this.mobTamingListener = new MobTamingListener(this, this.dailyQuestManager, this.weeklyQuestManager, this.playerManager);
        this.playerQuitListener = new PlayerQuitListener(this, this.playerManager, this.guiManager, this.minecartManager);
        this.playerJoinListener = new PlayerJoinListener(this, this.playerManager, this.guiManager);
        this.inventoryClickListener = new InventoryClickListener(this, this.guiManager);
        this.inventoryDragListener = new InventoryDragListener(this, this.guiManager);
        this.inventoryCloseListener = new InventoryCloseListener(this, this.guiManager);
    }

    public void loadCommands() {
        this.getCommand("quests").setExecutor(new QuestsCommand(this, this.dailyQuestManager, this.weeklyQuestManager, this.guiManager, this.rorysLibrary.getMessagingUtil()));
    }

    public void convertData() {
        FileConfiguration questDataConfig = this.questdataConfig.getConfig();

        if (questDataConfig.getConfigurationSection("current-quest") != null) {
            for (String key : questDataConfig.getConfigurationSection("current-quest").getKeys(false)) {
                questDataConfig.set("daily-quests.current-quest." + key, questDataConfig.get("current-quest." + key));
            }
            questDataConfig.set("daily-quests.current-quest-num", questDataConfig.getInt("current-quest-num"));
            questDataConfig.set("daily-quests.next-quest-time", questDataConfig.getString("next-quest-time"));

            for (String key : questDataConfig.getConfigurationSection("completed-ips").getKeys(false)) {
                questDataConfig.set("daily-quests.completed-ips." + key, questDataConfig.getString("completed-ips." + key));
            }

            questDataConfig.createSection("weekly-quests");

            questDataConfig.set("current-quest", null);
            questDataConfig.set("current-quest-num", null);
            questDataConfig.set("next-quest-time", null);
            questDataConfig.set("completed-ips", null);

            this.questdataConfig.saveConfig();
            this.questdataConfig.reloadConfig();
        }

        for (File file : (new File(this.getDataFolder().getPath() + File.separator + "playerdata")).listFiles()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            if (config.isSet("quest-progress")) {
                config.set("daily-quests.quest-progress", config.getInt("quest-progress"));
                config.set("daily-quests.quest-completed", config.getInt("quest-completed"));
                config.set("daily-quests.total-quests-completed", config.getInt("total-quests-completed"));

                config.set("quest-progress", null);
                config.set("quest-completed", null);
                config.set("total-quests-completed", null);

                try {
                    config.save(file);
                } catch (IOException e) {

                }
            }
        }
    }

    public BlockBreakListener getBlockBreakListener() {
        return blockBreakListener;
    }

    public BlockPlaceListener getBlockPlaceListener() {
        return blockPlaceListener;
    }

    public EntityDeathListener getEntityDeathListener() {
        return entityDeathListener;
    }

    public InventoryClickListener getInventoryClickListener() {
        return inventoryClickListener;
    }

    public InventoryCloseListener getInventoryCloseListener() {
        return inventoryCloseListener;
    }

    public InventoryDragListener getInventoryDragListener() {
        return inventoryDragListener;
    }

    public ItemCraftListener getItemCraftListener() {
        return itemCraftListener;
    }

    public ItemSmeltingListener getItemSmeltingListener() {
        return itemSmeltingListener;
    }

    public MobTamingListener getMobTamingListener() {
        return mobTamingListener;
    }

    public PlayerFishListener getPlayerFishListener() {
        return playerFishListener;
    }

    public PlayerJoinListener getPlayerJoinListener() {
        return playerJoinListener;
    }

    public PlayerQuitListener getPlayerQuitListener() {
        return playerQuitListener;
    }

    public VehicleEnterListener getVehicleEnterListener() {
        return vehicleEnterListener;
    }

    public VehicleExitListener getVehicleExitListener() {
        return vehicleExitListener;
    }

    public void unregisterAllQuestListeners() {
        HandlerList.unregisterAll(this.blockBreakListener);
        HandlerList.unregisterAll(this.blockPlaceListener);
        HandlerList.unregisterAll(this.entityDeathListener);
        HandlerList.unregisterAll(this.itemCraftListener);
        HandlerList.unregisterAll(this.itemSmeltingListener);
        HandlerList.unregisterAll(this.mobTamingListener);
        HandlerList.unregisterAll(this.playerFishListener);
        HandlerList.unregisterAll(this.vehicleEnterListener);
        HandlerList.unregisterAll(this.vehicleExitListener);

        this.blockBreakListener.setIsRegistered(false);
        this.blockPlaceListener.setIsRegistered(false);
        this.entityDeathListener.setIsRegistered(false);
        this.itemCraftListener.setIsRegistered(false);
        this.itemSmeltingListener.setIsRegistered(false);
        this.mobTamingListener.setIsRegistered(false);
        this.playerFishListener.setIsRegistered(false);
        this.vehicleEnterListener.setIsRegistered(false);
        this.vehicleExitListener.setIsRegistered(false);
    }

    public void registerListeners() {
        this.unregisterAllQuestListeners();

        if (this.dailyQuestManager.getCurrentQuest() != null) {
            this.dailyQuestManager.registerQuest(this.dailyQuestManager.getCurrentQuest());
        }
        if (this.weeklyQuestManager.getCurrentQuest() != null) {
            this.weeklyQuestManager.registerQuest(this.weeklyQuestManager.getCurrentQuest());
        }
    }
}
